FROM gentoo/stage3-amd64

RUN emerge-webrsync && \
    emerge distcc   && \
    rm -rf /var/db/repos/gentoo

CMD /usr/bin/distcc --user distcc --allow 0.0.0.0/0 --log-level=notice --no-detach --log-stderr

EXPOSE 3632